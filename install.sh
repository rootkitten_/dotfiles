#!/bin/bash

export PROFILE="$HOME/.zshrc"

prompt () {
  printf "$2: "
  read -e $1
}

main () {
  source common/install.sh
  source git/install.sh
  source zsh/install.sh
  source ohmyzsh/install.sh

  source firacode/install.sh

  source nvm/install.sh
  source spaceship-prompt/install.sh
  source vtop/install.sh
  source fzf/install.sh
  source ripgrep/install.sh
  source httpie/install.sh

  source docker/install.sh
  source docker-compose/install.sh
  source kubectl/install.sh
  source kubectx/install.sh
  source gcloud/install.sh

  source hyper/install.sh
  source firefox/install.sh
  source keybase/install.sh
  source vscode/install.sh
}

main
