sudo apt install -y git

prompt git_username 'Enter your git username'
prompt git_email 'Enter your git email'

git config --global user.name $git_username
git config --global user.email $git_email
