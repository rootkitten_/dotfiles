curl -L "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en" -o ./firefox/firefox.tar.bz2

mkdir ./firefox/bin
tar -xvjf ./firefox/firefox.tar.bz2 -C ./firefox/bin --strip-components=1
