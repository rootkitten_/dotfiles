curl https://raw.githubusercontent.com/ahmetb/kubectx/v0.6.3/kubectx -o ./kubectx/kubectx
curl https://raw.githubusercontent.com/ahmetb/kubectx/v0.6.3/kubens -o ./kubectx/kubens

chmod +x ./kubectx/kubectx
chmod +x ./kubectx/kubens

sudo mv ./kubectx/kubectx /usr/local/bin/kubectx
sudo mv ./kubectx/kubens /usr/local/bin/kubens
