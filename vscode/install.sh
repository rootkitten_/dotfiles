curl -L https://go.microsoft.com/fwlink/?LinkID=760868 -o ./vscode/vscode.deb
sudo apt install -y ./vscode/vscode.deb

mkdir -p ~/.config/Code/User
rm ~/.config/Code/User/settings.json

ln -s $PWD/vscode/settings.json ~/.config/Code/User/settings.json
