curl \
  -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" \
  -o ./docker-compose/docker-compose

chmod +x ./docker-compose/docker-compose

sudo mv ./docker-compose/docker-compose /usr/local/bin/docker-compose
