module.exports = {
  config: {
    fontFamily: '"Fira Code", Menlo, "DejaVu Sans Mono", Consolas, "Lucida Console", monospace',
    cursorBlink: true,
    showHamburgerMenu: true,
    showWindowControls: true,
    webGLRenderer: false
  },
  plugins: ['hyper-font-ligatures', 'hyper-snazzy'],
  keymaps: {}
};
