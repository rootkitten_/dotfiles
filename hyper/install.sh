curl -L https://releases.hyper.is/download/deb -o ./hyper/hyper.deb
sudo apt install -y ./hyper/hyper.deb

hyper install hyper-font-ligatures
hyper install hyper-snazzy

rm ~/.hyper.js

ln -s $PWD/hyper/.hyper.js ~/.hyper.js
